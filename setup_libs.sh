#!/bin/bash

#/* ANTI-BULLSHIT SOFTWARE LICENSE (v 1.0) */
#/* This is anti-bullshit software, released for free use by individuals and */ 
#/* organizations that do not force their political opinions on everyone by */ 
#/* injecting it everywhere */
#/* Permission is hereby granted, free of charge, to any person or organization */ 
#/* (the "User") obtaining a copy of this software and associated documentation */ 
#/* files (the "Software"), to use, copy, modify, merge, distribute, and/or sell */ 
#/* copies of the Software, subject to the following condition: */
#/*    - Don't force/inject politics */ 
#/*    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY */ 
#/* KIND, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS */ 
#/* FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS BE */ 
#/* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF */ 
#/* CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE */ 
#/* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

#get the repos of unikraft in a json array
unikraft_repos="$(curl -s --header "Accept:application/vnd.github.inertia-preview+json" https://api.github.com/orgs/unikraft/repos | jq "map(.url)")"

if test $? != 0; then
	echo something went wrong when querying the github api
	exit 1
fi

#Go at it like a retard by using grep because I'm a lazy fuck, also trim <"> and <,> and < >
libs=$(echo "$unikraft_repos" | egrep -o '.*/lib-.*$' | tr -d ' ,"')


lib_directory=""

if test $# == 0; then
	lib_directory="$HOME/workspace/libs"
	echo "You have not given an argument to this script, the libs will be cloned to $lib_directory"
	read -p "Do you agree?[y/n]" yn
	case $yn in
		y ) ;;
		n ) echo "Relaunch with an argument"; exit 1;;
		* ) echo "y or n please";;
	esac
else
	lib_directory="$1"
fi



cd "$lib_directory"
for lib in $libs; do
	lib_name="$(echo "$lib" | egrep -o 'unikraft/.*$')"
	git clone "https://www.github.com/$lib_name"
done
